/**
 * @file D3DVideoObject.cpp
 * @author Ahonen
 *
 * @brief Video object (using libtheoraplayer).
 */

#include "D3DVideoObject.h"


// Static variables
bool D3DVideoObject::ourIsInitialized = false;
TheoraVideoManager* D3DVideoObject::ourManager = NULL;


void D3DVideoObject::Initialize()
{
    DBG( "Initializing D3DVideoObject" );
    ourManager = new TheoraVideoManager();
    ourIsInitialized = true;
}

bool D3DVideoObject::IsInitialized()
{
    return ourIsInitialized;
}

void D3DVideoObject::CleanUp()
{
    DBG( "Cleaning up D3DVideoObject" );
    ourIsInitialized = false;
    
    delete ourManager;
    ourManager = NULL;
}

D3DVideoObject* D3DVideoObject::Open( char const* filename )
{
    D3DVideoObject* obj = new D3DVideoObject();
	try
	{
		obj->myClip = ourManager->createVideoClip( filename, TH_BGRA );
	}
	catch (...)
	{
		DBG("File could not be opened: %s", filename);;
	}

    if ( !obj->myClip )
    {
        delete obj;
        return NULL;
    }

    // No looping by default
    obj->myClip->setAutoRestart( false );

    // Wait until frame cache is filled
    while ( obj->myClip->getNumReadyFrames() != obj->myClip->getNumPrecachedFrames() );
    
	obj->myWidth = obj->myClip->getWidth();
	obj->myHeight = obj->myClip->getHeight();

    return obj;
}

D3DVideoObject* D3DVideoObject::Restore( char const* buffer, int size )
{
    D3DVideoObject* obj = new D3DVideoObject();
    obj->Unserialize( buffer, size );
    return obj;
}

D3DVideoObject::D3DVideoObject():
    D3DObject(),
    myClip( NULL ),
    myTexture( NULL ),
    myIsAutoplaying( false ),
    myDelayedUpdate( false )
{
    if ( !ourIsInitialized )
    {
        Initialize();
    }

    DBG( "D3DVideoObject created" );
}

D3DVideoObject::~D3DVideoObject()
{
    DBG( "D3DVideoObject destroyed" );

    if ( myClip )
    {
        ourManager->destroyVideoClip( myClip );
        myClip = NULL;
    }

    if ( myTexture )
    {
        myTexture->Release();
        myTexture = NULL;
    }
}

void D3DVideoObject::SetLooping( bool looping )
{
    if ( !myClip ) return;

    myClip->setAutoRestart( looping );
}

bool D3DVideoObject::IsLooping() const
{
    if ( !myClip ) return false;

    return myClip->getAutoRestart();
}

void D3DVideoObject::SetFPS( float fps )
{
    if ( !myClip ) return;

    myClip->setPlaybackSpeed( fps / myClip->getFPS() );
}

float D3DVideoObject::GetFPS() const
{
    if ( !myClip ) return 0.f;

    return myClip->getPlaybackSpeed() * myClip->getFPS();
}

bool D3DVideoObject::NextFrame()
{
    if ( !myClip ) return false;

    myClip->updateToNextFrame();

    return !myClip->isDone();
}

void D3DVideoObject::Autoplay()
{
    myIsAutoplaying = true;
}

bool D3DVideoObject::IsAutoplaying() const
{
    return myIsAutoplaying;
}

void D3DVideoObject::StopAutoplay()
{
    myIsAutoplaying = false;
}

int D3DVideoObject::GetWidth() const
{
    if ( !myClip ) return 0;

    return myClip->getWidth();
}

int D3DVideoObject::GetHeight() const
{
    if ( !myClip ) return 0;

    return myClip->getHeight();
}

void D3DVideoObject::Start()
{

    if ( !myClip ) return;

    // Go to the first frame and get texture
    myClip->updateToNextFrame();
    UpdateTexture();
}

void D3DVideoObject::Update()
{
    if ( !myClip ) return;

    if ( !GetD3D() )
    {
        // Direct3D device not yet available, delay update
        DBG( "Delay" );
        myDelayedUpdate = true;
        return;
    }

    if ( myIsAutoplaying )
    {
        // Autoplay video
        myClip->update( GetScreen()->frameDelay );
    }

    // Change texture if frame has changed
    UpdateTexture();
}

void D3DVideoObject::Render()
{
    if ( !myClip ) return;
    
    if ( myDelayedUpdate )
    {
        DBG( "Delayed update" );
        // Run delayed update now that Direct3D device is definitely available
        Update();
        myDelayedUpdate = false;
    }

    if ( !myTexture )
    {
        DBG( "No texture" );
        return;
    }

    // Render texture to screen
    D3DObject::Render( myTexture, myClip->getWidth(), myClip->getHeight() );
}

int D3DVideoObject::Serialize( char* buffer, int bufsize )
{
    char* bufStart = buffer;

    int num = D3DObject::Serialize( buffer, bufsize );
    buffer += num;
    
    SERIALIZE_S( myClip->getName().c_str() );
    float time = myClip->getTimePosition();
    SERIALIZE( time );
    float speed = myClip->getPlaybackSpeed();
    SERIALIZE( speed );
    bool loop = myClip->getAutoRestart();
    SERIALIZE( loop );
    SERIALIZE( myIsAutoplaying );
    SERIALIZE( myDelayedUpdate );

    return buffer - bufStart;
}

int D3DVideoObject::Unserialize( char const* buffer, int size )
{
    char const* bufStart = buffer;

    int num = D3DObject::Unserialize( buffer, size );
    buffer += num;

    char* filename;
    UNSERIALIZE_S( filename );
    float time;
    UNSERIALIZE( time );
    float speed;
    UNSERIALIZE( speed );
    bool loop;
    UNSERIALIZE( loop );
    UNSERIALIZE( myIsAutoplaying );
    UNSERIALIZE( myDelayedUpdate );

    // Load video
    myClip = ourManager->createVideoClip( filename, TH_BGRA );
    
    delete[] filename;
    filename = NULL;

    if ( !myClip )
    {
        DBG( "D3DVideoObject clip not found" );
    }
    else
    {
        // Set video properties
        myClip->setAutoRestart( loop );
        myClip->setPlaybackSpeed( speed );
        myClip->seek( time );

        // Wait until frame cache is filled
        while ( myClip->getNumReadyFrames() != myClip->getNumPrecachedFrames() );

        UpdateTexture();
    }

    return buffer - bufStart;
}

void D3DVideoObject::UpdateTexture()
{    
    if ( !myClip ) return;

    TheoraVideoFrame* frame = myClip->getNextFrame();

    if ( frame )
    {
        // New frame, let's update the texture
        if ( !myTexture )
        {
            // Create texture
            myTexture = CreateTexture( frame->getBuffer(), frame->getWidth(), frame->getHeight() );
        }
        else
        {
            // Update texture
            SetTextureData( myTexture, frame->getBuffer(), frame->getWidth(), frame->getHeight() );
        }
        
        // Pop frame from queue
        myClip->popFrame();
    }
}




int D3DVideoObject_Manager::Dispose( char const* address, bool force )
{
    delete (D3DVideoObject*)address;
    return 1;
}

char const* D3DVideoObject_Manager::GetType()
{
    return "D3DVideoObject";
}

int D3DVideoObject_Manager::Serialize( char const* address, char* buffer, int bufsize )
{
    return ((D3DVideoObject*)address)->Serialize( buffer, bufsize );
}

void D3DVideoObject_Manager::Unserialize( int key, char const* buffer, int size )
{
    D3DVideoObject* obj = D3DVideoObject::Restore( buffer, size );

    if ( obj )
    {
        GetAGS()->RegisterUnserializedObject( key, obj, this );
    }
}

