/**
 * @file D3DVideoObject.h
 * @author Ahonen
 *
 * @brief Video object (using libtheoraplayer).
 */

#ifndef D3D_D3DVIDEOOBJECT_H
#define D3D_D3DVIDEOOBJECT_H

#include "D3DObject.h"
#define THEORAVIDEO_STATIC
#include <TheoraPlayer.h>

class D3DVideoObject : public D3DObject
{
public:
    static void Initialize();

    static bool IsInitialized();

    static void CleanUp();

    static D3DVideoObject* Open( char const* filename );

    static D3DVideoObject* Restore( char const* buffer, int size );

    virtual ~D3DVideoObject();
    
    void SetLooping( bool looping );

    bool IsLooping() const;

    void SetFPS( float fps );

    float GetFPS() const;

    bool NextFrame();

    void Autoplay();

    bool IsAutoplaying() const;

    void StopAutoplay();

    virtual int GetWidth() const;

    virtual int GetHeight() const;

    virtual void Start();

    virtual void Update();

    virtual void Render();

    virtual int Serialize( char* buffer, int bufsize );

    virtual int Unserialize( char const* buffer, int size );

private:
    void UpdateTexture();

    static bool ourIsInitialized;
    static TheoraVideoManager* ourManager;

    TheoraVideoClip* myClip;
    IDirect3DTexture9* myTexture;

    bool myIsAutoplaying;
    bool myDelayedUpdate;

    // Blocks
    D3DVideoObject();
    D3DVideoObject( D3DVideoObject const& );
    D3DVideoObject& operator=( D3DVideoObject const& );
};



class D3DVideoObject_Manager : public IAGSScriptManagedObject,
                               public IAGSManagedObjectReader
{
public:
    virtual int Dispose( char const* address, bool force );

    virtual char const* GetType();

    virtual int Serialize( char const* address, char* buffer, int bufsize );

    virtual void Unserialize( int key, char const* buffer, int size );
};

#endif
